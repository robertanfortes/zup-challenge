(function () {
  'use strict';
  angular.module('zup.shotsList')
    .directive('shotComponent', function() {
      return {
          restrict: 'E',
          replace:'true',
          templateUrl: '../partials/shotComponent.html'
      };
    });
})();
