(function () {
  'use strict';
  angular.module('zup.shotsList')
  .directive('shotBiggerComponent', function() {
    return {
        restrict: 'E',
        replace:'true',
        templateUrl: '../partials/shotBiggerComponent.html'
    };
  });
})();
