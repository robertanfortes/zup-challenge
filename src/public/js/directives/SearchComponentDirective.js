(function () {
  'use strict';
  angular.module('zup.shotsList')
    .directive('searchComponent', function() {
      return {
          restrict: 'E',
          replace:'true',
          templateUrl: '../partials/searchComponent.html'
      };
    });
})();
