(function () {
/* Services */

angular.module('zup.shotsList')
.factory('postLikes', ['$http', function ($http){
	var postLikes = {};

	postLikes.post = $http.post('https://dribbble.co/shots/:id/like').then(function(resp){
			return resp.data;
	});


	postLikes.all = function(){
		return postLikes.post;
	};

  return postLikes;


}])

}());
