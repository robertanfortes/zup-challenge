(function () {
/* Services */

angular.module('zup.shotsList')
.factory('shotJson', ['$http', function ($http){
	var shotJson = {};

	shotJson.get = $http.get('https://api.dribbble.com/v1/shots?access_token=a1fd0e630e04a2cf342e95a49d733b89c59600e27821cf852a09ee1c60753773').then(function(resp){
			return resp.data;

	});
	shotJson.all = function(){
		return shotJson.get;
	};
	return shotJson;

}])

}());
