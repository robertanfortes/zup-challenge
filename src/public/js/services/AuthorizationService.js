(function () {
/* Services */

angular.module('zup.shotsList')
.factory('authJson', ['$http', function ($http){
	var postAuth = {};
  var getAuth = {};
  var getAuthJson = {};
  getAuth.get = $http.get('https://dribbble.com/oauth/authorize?client_id=9727336d8e2b7e35cd3e45e3c335242dcee495a397ee72f4568a8e3e33d713f5&scope=public+write').then(function(resp){
			return resp.data;
	});
	postAuth.post = $http.post('https://dribbble.com/oauth/token?client_id=9727336d8e2b7e35cd3e45e3c335242dcee495a397ee72f4568a8e3e33d713f5&scope=public+write&client_secret=fb7c1f85a249c9de8ac67acd695e1c0eb50757e750a3804eab977bcbe2ccf49d&code=89a43b4e2264f62055e749c3b0b9d50dc236af0c038f8ca4e39f04e30ec51f2f').then(function(resp){
			return resp.data;
	});

	getAuthJson.get = $http.get('https://api.dribbble.com/v1/user?access_token=a1fd0e630e04a2cf342e95a49d733b89c59600e27821cf852a09ee1c60753773&scope=public+write').then(function(resp){
			return resp.data;
	});

  getAuth.all = function(){
		return getAuth.get;
	};
	postAuth.all = function(){
		return postAuth.post;
	};
  getAuthJson.all = function(){
		return getAuthJson.get;
	};
	return getAuth;
  return postAuth;
  return getAuthJson;

}])

}());
