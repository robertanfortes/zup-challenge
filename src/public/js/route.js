(function () {

  angular.module('zup.route', ['ui.router'])
  .config(function ($stateProvider, $urlRouterProvider, $locationProvider) {

  $stateProvider
     .state('index', {
      url: '/',
      templateUrl: 'partials/shots.html',
      controller:  'ShotsCtrl',
      controllerAs: 'vm'
    })

    .state('shotItem', {
     url: '/shots/:shotID',
     templateUrl: 'partials/shotsItem.html',
     controller:  'ModalCtrl',
     controllerAs: 'vm'
   });


    $urlRouterProvider.otherwise('/');
    $locationProvider.html5Mode(true);
  });
})();
