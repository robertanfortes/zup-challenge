(function () {
  'use strict';
  angular.module('zup.shotsList')
    .controller('ShotsCtrl',Controller);
      function Controller(ModalService,$scope,$http,shotJson,authJson,postLikes,$location,dataService) {

        $scope.leader = {
            title: undefined,
            images: undefined,
            description: undefined,
            likes_count: undefined,
            user:{
              avatar_url: undefined,
              name: undefined
            }
          };

        // get all dribbles
        shotJson.all().then(function(resp) {
            $scope.shots = resp;
        });
        authJson.all().then(function(resp) {
            $scope.auths = resp;
        });
        postLikes.all().then(function(resp) {
            $scope.likes = resp;
        });
        // update dataService to display selected dribbble at another view
        $scope.update = function(shot) {
           _update(shot, $scope.leader);
           dataService.set($scope.leader);
         }
        // $scope.update = function(shot) {
        //  angular.copy(shot, $scope.leader);
        //  dataService.set($scope.leader);
        // };

        // function to update dataservice
        function _update(srcObj, destObj) {
            for (var key in destObj) {
              if(destObj.hasOwnProperty(key) && srcObj.hasOwnProperty(key)) {
                destObj[key] = srcObj[key];
              }
            }
          }

        // display selected dribbble inside modal content
        var vm = this;
        vm.openModal = openModal;
        vm.closeModal = closeModal;
        initController();

        function initController() {
            vm.bodyText = '';
        }

        function openModal(id){
            ModalService.Open(id);
        }

        function closeModal(id){
            ModalService.Close(id);
        }
    }

})();
