### O projeto ###

Aplicação web contendo as principais funcionalidades do website dribbble.com, utilizando sua API pública.

### Quick start

# instalar dependencias via npm
$ npm install

# start do servidor
$ npm start

ir para [http://localhost:8080](http://localhost:8080)

## Dependencies

* `node` and `npm`

## Developing

### Build files

* single run: `npm run build`
* build files and watch: `npm start`

## Testing

#### 1. Unit Tests

* single run: `npm test`
* live mode (TDD style): `npm run test-watch`
